package com.pligor.zerobill_request_response

import play.api.libs.json.Json

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
object FortumoReceiptResponse {
  implicit val jsonFormat = Json.format[FortumoReceiptResponse]
}

case class FortumoReceiptResponse(fortumo_receipt_received: Long,
                           service_id: String,
                           product_name: String) {

}

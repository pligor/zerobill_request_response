package com.pligor.zerobill_request_response

import play.api.libs.json._
import scala.collection.immutable.ListMap
import play.api.libs.json.JsObject
import play.api.libs.json.JsString

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
object LoginRequest {
  implicit val jsonFormat = Json.format[LoginRequest]

  private def keyValueToKeyJson(kv: (String, AnyRef)) = {
    kv._2 match {
      case null => (kv._1, JsNull)
      case value: String => (kv._1, JsString(value))
    }
  }

  def mapToAttributes(atts: Map[String, AnyRef]): JsObject = {
    JsObject(
      atts.map(keyValueToKeyJson).toList
    )
  }
}

/**
 * @param provider facebook, linkedin, googleplus, twitter, etc.
 * @param unique_id this specifies a certain user in the social network, cannot change
 * @param access_token the access token that is generated after successful sign in to social network
 * @param consumer_secret this is not the same as api secret
 */
case class LoginRequest(provider: String,
                        unique_id: String,
                        access_token: String,
                        attributes: JsObject,
                        consumer_secret: Option[String]) {
  def attributesToMap: ListMap[String, Option[String]] = {
    val seq = attributes.fieldSet.map(tuple => tuple._1 -> tuple._2.asOpt[JsString].map(_.value)).toSeq

    ListMap(seq: _ *)
  }

  def isReferringToSameUser(that: LoginRequest): Boolean = {
    this.provider == that.provider && this.unique_id == that.unique_id
  }
}

package com.pligor.zerobill_request_response

import scala.collection.mutable
import play.api.libs.json.JsObject

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
case class PhoneNumberInfo(phoneNumberFreeStyle: String, wired: Boolean)

case class OperatorInfo(operatorId: Int, operatorName: String, phoneNumberInfos: mutable.Buffer[PhoneNumberInfo])

case class OperatorInfoJson(operatorId: Int, operatorName: String, phoneNumberInfosJsObj: JsObject)

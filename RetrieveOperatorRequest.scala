package com.pligor.zerobill_request_response

import com.google.i18n.phonenumbers.Phonenumber.PhoneNumber
import play.api.libs.json.Json
import com.pligor.phonenumber.PhoneNumberHelper._

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
object RetrieveOperatorRequest {
  implicit val jsonFormat = Json.format[RetrieveOperatorRequest]
}

case class RetrieveOperatorRequest(phone_number: PhoneNumber, phone_number_free_style: String) {

}

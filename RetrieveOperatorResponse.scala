package com.pligor.zerobill_request_response

import play.api.libs.json.Json

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
object RetrieveOperatorResponse {
  implicit val jsonFormat = Json.format[RetrieveOperatorResponse]
}

case class RetrieveOperatorResponse(operator_id: Int, operator_name: String, wired: Boolean) {

}

package com.pligor.zerobill_request_response

import play.api.libs.json.Json
import com.pligor.generic.EmailHelper._
import com.pligor.phonenumber.PhoneNumberHelper._
import com.google.i18n.phonenumbers.Phonenumber.PhoneNumber

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
object SubmitPromoCodeRequest {
  implicit val jsonFormat = Json.format[SubmitPromoCodeRequest]
}

case class SubmitPromoCodeRequest(promo_code: String, identification: String) {
  def isIdentificationValid: Boolean = {
    isValidEmailAddress(identification) || isValidPhoneNumber(identification)
  }

  def workWithValidIdentification: Option[Either[String, PhoneNumber]] = {
    if (isValidEmailAddress(identification)) {
      Some(Left(identification))
    } else {
      parsePhoneString(identification).map(Right.apply)
    }
  }
}

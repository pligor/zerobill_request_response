package com.pligor.zerobill_request_response

import play.api.libs.json.Json
import com.google.i18n.phonenumbers.Phonenumber.PhoneNumber
import com.pligor.phonenumber.PhoneNumberHelper._

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
protected object MccMnc {
  implicit val jsonFormat = Json.format[MccMnc]
}

protected case class MccMnc(mcc: Int, mnc: Int)

object RetrieveSmsInfoRequest {
  implicit val jsonFormat = Json.format[RetrieveSmsInfoRequest]
}

case class RetrieveSmsInfoRequest(operator_id: Option[Int],
                                  phone_number: Option[PhoneNumber],
                                  mcc_mnc: Option[MccMnc]) {
  val isAtLeastOneDefined = {
    operator_id.isDefined || phone_number.isDefined || mcc_mnc.isDefined
  }
}

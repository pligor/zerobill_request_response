package com.pligor.zerobill_request_response

import play.api.libs.json.Json

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
object PutFortumoDeviceReceiptRequest {
  implicit val jsonFormat = Json.format[PutFortumoDeviceReceiptRequest]
}

case class PutFortumoDeviceReceiptRequest(payment_code: String,
                                          fortumo_user_id: String,
                                          service_id: String,
                                          product_name: String,
                                          payed: Long) {

}
